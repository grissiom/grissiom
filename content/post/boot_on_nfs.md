---
title: "用 NFS 启动时遇到的版本问题"
date: 2021-02-11T22:10:34+08:00
draft: false
tags: ["linux", "nfs", "embedded"]
---

最近在玩 [STM32MP1-DK1](https://www.st.com/en/evaluation-tools/stm32mp157a-dk1.html) 的板子，基本的配置 boot 起来之后当然第一件事就是上 nfs，这样调试起来会方便很多。从网上搜了一个教程，把 `bootfs/mmc0_extlinux/extlinux.conf` 里面的 uboot 配置改为：

```
APPEND root=/dev/nfs rw console=ttySTM0,115200 nfsroot=/home/grissiom/nfs ip=192.168.3.200:192.168.3.109:192.168.3.1:255.255.255.0::eth0
```

但是启动之后总是找不到 rootfs：

```
[    7.684146] stm32-dwmac 5800a000.ethernet eth0: Link is Up - 1Gbps/Full - flow control off
[    7.712451] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[    7.742426] IP-Config: Complete:
[    7.744202]      device=eth0, hwaddr=00:80:e1:42:7a:01, ipaddr=192.168.3.200, mask=255.255.255.0, gw=192.168.3.1
[    7.754439]      host=192.168.3.200, domain=, nis-domain=(none)
[    7.760334]      bootserver=192.168.3.109, rootserver=192.168.3.109, rootpath=
[    7.768810] ALSA device list:
[    7.770545]   #0: STM32MP1-DK
[   38.242424] vref: supplied by vdd
[   38.244457] usb33: supplied by vdd_usb
[   38.248160] vref: disabling
[   38.250852] vdda: disabling
[  103.526732] VFS: Unable to mount root fs via NFS, trying floppy.
[  103.531929] VFS: Cannot open root device "nfs" or unknown-block(2,0): error -6
[  103.538642] Please append a correct "root=" boot option; here are the available partitions:
[  103.547099] 0100           65536 ram0 
[  103.547104]  (driver?)
[  103.553067] 0101           65536 ram1 
[  103.553071]  (driver?)
……
[  103.705998] Kernel panic - not syncing: VFS: Unable to mount root fs on unknown-block(2,0)
[  103.714286] CPU: 1 PID: 1 Comm: swapper/0 Not tainted 5.4.56 #1
[  103.720215] Hardware name: STM32 (Device Tree Support)
[  103.725403] [<c0112490>] (unwind_backtrace) from [<c010d71c>] (show_stack+0x10/0x14)
[  103.733152] [<c010d71c>] (show_stack) from [<c0abaf34>] (dump_stack+0xb0/0xc4)
[  103.740398] [<c0abaf34>] (dump_stack) from [<c0121a48>] (panic+0x11c/0x31c)
[  103.747380] [<c0121a48>] (panic) from [<c0f0150c>] (mount_block_root+0x1cc/0x27c)
[  103.754886] [<c0f0150c>] (mount_block_root) from [<c0f01828>] (mount_root+0x124/0x148)
[  103.762830] [<c0f01828>] (mount_root) from [<c0f019cc>] (prepare_namespace+0x180/0x1c4)
[  103.770865] [<c0f019cc>] (prepare_namespace) from [<c0ad2500>] (kernel_init+0x8/0x110)
[  103.778808] [<c0ad2500>] (kernel_init) from [<c01010e8>] (ret_from_fork+0x14/0x2c)
[  103.786396] Exception stack(0xdf083fb0 to 0xdf083ff8)
```

同一个网段内另外一台机器挂这个 nfs 就没有任何问题，所以排除是本地防火墙的问题。万般无奈，上 wireshark 抓包看看：

```
136	34.082498838	192.168.3.200	192.168.3.109	Portmap	134	V2 GETPORT Call (Reply In 137) MOUNT(100005) V:1 UDP
137	34.082757346	192.168.3.109	192.168.3.200	Portmap	70	V2 GETPORT Reply (Call In 136) Port:20048
138	34.083500464	192.168.3.200	192.168.3.109	MOUNT	82	V1 NULL Call (Reply In 139)
139	34.083701401	192.168.3.109	192.168.3.200	MOUNT	66	V1 NULL Reply (Call In 138)
140	34.084122051	192.168.3.200	192.168.3.109	MOUNT	142	V1 MNT Call (Reply In 141) /home/grissiom/nfs
141	34.085351472	192.168.3.109	192.168.3.200	MOUNT	102	V1 MNT Reply (Call In 140)
142	34.086159847	192.168.3.200	192.168.3.109	Portmap	134	V2 GETPORT Call (Reply In 143) NFS(100003) V:2 UDP
143	34.086371442	192.168.3.109	192.168.3.200	Portmap	70	V2 GETPORT Reply (Call In 142) PROGRAM_NOT_AVAILABLE
163	39.108825395	192.168.3.200	192.168.3.109	Portmap	134	V2 GETPORT Call (Reply In 164) MOUNT(100005) V:1 UDP
164	39.109080861	192.168.3.109	192.168.3.200	Portmap	70	V2 GETPORT Reply (Call In 163) Port:20048
165	39.109736874	192.168.3.200	192.168.3.109	MOUNT	82	V1 NULL Call (Reply In 166)
166	39.109866176	192.168.3.109	192.168.3.200	MOUNT	66	V1 NULL Reply (Call In 165)
167	39.110246498	192.168.3.200	192.168.3.109	MOUNT	142	V1 MNT Call (Reply In 168) /home/grissiom/nfs
168	39.110707757	192.168.3.109	192.168.3.200	MOUNT	102	V1 MNT Reply (Call In 167)
169	39.111464388	192.168.3.200	192.168.3.109	Portmap	134	V2 GETPORT Call (Reply In 170) NFS(100003) V:2 UDP
170	39.111676119	192.168.3.109	192.168.3.200	Portmap	70	V2 GETPORT Reply (Call In 169) PROGRAM_NOT_AVAILABLE
184	49.188799790	192.168.3.200	192.168.3.109	Portmap	134	V2 GETPORT Call (Reply In 185) MOUNT(100005) V:1 UDP
185	49.189057614	192.168.3.109	192.168.3.200	Portmap	70	V2 GETPORT Reply (Call In 184) Port:20048
186	49.189743486	192.168.3.200	192.168.3.109	MOUNT	82	V1 NULL Call (Reply In 187)
187	49.189874591	192.168.3.109	192.168.3.200	MOUNT	66	V1 NULL Reply (Call In 186)
188	49.190289862	192.168.3.200	192.168.3.109	MOUNT	142	V1 MNT Call (Reply In 189) /home/grissiom/nfs
189	49.190747374	192.168.3.109	192.168.3.200	MOUNT	102	V1 MNT Reply (Call In 188)
190	49.191591585	192.168.3.200	192.168.3.109	Portmap	134	V2 GETPORT Call (Reply In 191) NFS(100003) V:2 UDP
191	49.191801703	192.168.3.109	192.168.3.200	Portmap	70	V2 GETPORT Reply (Call In 190) PROGRAM_NOT_AVAILABLE
```

过滤 IP 地址之后发现，板子先会使用 V1 的方式尝试挂载，然后再尝试用 V2 挂载。因为本地的服务端不支持 V2，所以板子就认为有问题重来了…… 改下 uboot 选项，直接制定 nfs 的版本为 V4：

```
APPEND root=/dev/nfs rw console=ttySTM0,115200 nfsroot=/home/grissiom/nfs,vers=4,tcp ip=192.168.3.200:192.168.3.109:192.168.3.1:255.255.255.0::eth0
```

果然就好了：

```
[    7.684030] stm32-dwmac 5800a000.ethernet eth0: Link is Up - 1Gbps/Full - flow control off
[    7.712327] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
[    7.742322] IP-Config: Complete:
[    7.744100]      device=eth0, hwaddr=00:80:e1:42:7a:01, ipaddr=192.168.3.200, mask=255.255.255.0, gw=192.168.3.1
[    7.754349]      host=192.168.3.200, domain=, nis-domain=(none)
[    7.760252]      bootserver=192.168.3.109, rootserver=192.168.3.109, rootpath=
[    7.768794] ALSA device list:
[    7.770473]   #0: STM32MP1-DK
[    7.794264] VFS: Mounted root (nfs4 filesystem) on device 0:19.
[    7.809775] devtmpfs: mounted
[    7.814110] Freeing unused kernel memory: 1024K
[    7.842684] Run /sbin/init as init process
INIT: version 2.96 booting
Starting udev
[    8.607302] udevd[136]: starting version 3.2.9
[    8.725165] udevd[138]: starting eudev-3.2.9

Poky (Yocto Project Reference Distro) 3.1.5 stm32mp1 /dev/ttySTM0

stm32mp1 login: [   38.242320] vref: supplied by vdd
```

需要注意的是，NFSv4 不支持 UDP，只能通过 TCP 来通讯[^1]，所以需要中加入 `tcp` 选项。



[^1]: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/4/html/reference_guide/ch-nfs