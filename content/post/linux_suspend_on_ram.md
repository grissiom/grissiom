---
title: "Linux 无法休眠事件"
date: 2021-02-10T23:20:09+08:00
draft: false
tags: ["linux"]
---

平时都是使用 suspend to RAM 功能“关机”。这样就能自然的保存一些工作的进展，一些未读文章啥的也不用担心找不到了，简直是拯救懒癌的必备良药。

不过昨天忽然发现 Linux 忽然无法休眠了。具体的现象是休眠之后黑屏，但是风扇会忽然又启动。动键盘鼠标无反应，按电源建屏亮了，恢复之前的状态。感觉这个东西不对头，所以需要 debug
一下。打开 Konsole，`dmesg` 一下，果然有东西：

```dmesg
[ 9682.739958] Freezing user space processes ... (elapsed 0.001 seconds) done.
[ 9682.741571] OOM killer disabled.
[ 9682.741571] Freezing remaining freezable tasks ... (elapsed 0.001 seconds) done.
[ 9682.742822] printk: Suspending console(s) (use no_console_suspend to debug)
[ 9682.743623] serial 00:01: disabled
[ 9682.762308] sd 0:0:0:0: [sda] Synchronizing SCSI cache
[ 9682.762469] sd 0:0:0:0: [sda] Stopping disk
[ 9682.789980] PM: pci_pm_suspend(): hcd_pci_suspend+0x0/0x30 [usbcore] returns -16
[ 9682.789983] PM: dpm_run_callback(): pci_pm_suspend+0x0/0x160 returns -16
[ 9682.789983] PM: Device 0000:00:14.0 failed to suspend async: error -16
[ 9683.394247] PM: Some devices failed to suspend, or early wake event detected
[ 9683.397574] serial 00:01: activated
[ 9683.397981] sd 0:0:0:0: [sda] Starting disk
[ 9683.407379] nvme nvme0: 8/0/0 default/read/poll queues
```

看以 `PM:` 开头的几行，果然有问题，有设备不支持休眠或者休眠出错了。遍历下主机上插着的外设，有一个 USB 口的 TF 卡读卡器比较可疑，拔掉再休眠，果然就好了。
