---
title: "About"
date: 2021-02-10
lastmod: 2021-02-10
menu: "main"
weight: 50

# you can close something for this content if you open it in config.toml.
comment: false
mathjax: false
---

欢迎来到我的个人主页！

主要是记录一些心得体会。
